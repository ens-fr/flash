# Erreur en console

Voici un extrait d'utilisation de la console Python :

```pycon
>>> a = sqrt(5)
Traceback (most recent call last):
  File "<interactive input>", line 1, in <module>
NameError: name 'sqrt' is not defined
```

=== "Questions"

1. Quelle est l'erreur indiquée par Python ?
2. Comment corriger cette erreur ?

??? done "1. Solution"
    Le message indique une erreur de nom, le nom `sqrt` n'est pas défini. On ne peut donc pas l'utiliser.

??? done "2. Solution"
    Il suffit d'écrire `#!python from math import sqrt`, et de recommencer le calcul de `a`.
