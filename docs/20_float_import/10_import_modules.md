# Import modules

On considère le script suivant :

```python
...

a = sqrt(2) / 2
b = pi / 4
```

=== "Question"

    Comment faire l'import nécessaire, à la place de `...`, pour que le script s'exécute sans erreur ?

??? done "Solution"
    Il est possible d'écrire `#!python from math import *`

    Mais il vaut mieux écrire `#! python from math import sqrt, pi`

    - On n'importe que ce dont on a besoin !
    - Avec `#!python from math import *` on remplace la fonction `pow` par une autre... qui est moins bien pour le calcul modulaire, utile en terminale maths expertes.
