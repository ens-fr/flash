# Augmenter tant que

On considère la fonction suivante :

````python
def mystère(a):
    while a != 100:
        a = a + 1
    return a
````

=== "Questions"
    1. Que renvoie `mystère(2)` ?
    2. Cette fonction renvoie-t-elle toujours quelque chose ?
        - _Justifier brièvement_.
