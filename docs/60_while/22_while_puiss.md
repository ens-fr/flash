# Repunit

On considère la fonction suivante :

```python
def mystère(n):
    s = 1 
    while s <= 10**n:
        s = s * 10 + 1
    return s
```

=== "Question"
    Que renvoie `mystère(3)` ?
