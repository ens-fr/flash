# Tant de mystères

On considère la fonction suivante :

```python
def mystère(u):
    while u <= 100:
        u = 10*u + 3
    return u
```

=== "Questions"
    1. Que renvoie `mystère(0)` ?
    2. Vrai ou Faux : `mystère(u)` peut renvoyer `123`.
    3. Vrai ou Faux : `mystère(u)` peut renvoyer `132`.
    4. Vrai ou Faux : `mystère(u)` peut renvoyer `93`.
