# Tant de puissance

On considère la fonction suivante :

```python
def mystère(a):
    n = 0
    while 2**n <= a:
        n = n + 1
    return n
```

=== "Questions"
    1. Vrai ou Faux : `mystère(10)` renvoie 4.
    2. Que renvoie `mystère(100)` ?
