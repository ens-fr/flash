# Trop de deux

On considère la fonction suivante :

```python
def mystère(n):
    while n > 10:
        n = n + 2
    return n
```

=== "Questions"
    1. Que renvoie `mystère(7)` ?
    2. Que renvoie `mystère(15)` ?
