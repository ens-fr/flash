# Fonction Mystère par morceaux

On considère la fonction suivante :

````python
from math import sqrt

def mystère(x):
    if x > 0:
        return sqrt(x)
    elif x != 0:
        return 1/x
    else:
        return 1
````

=== "Questions"
    1. Que renvoie `mystère(16)` ?
    2. Que renvoie `mystère(0)` ?
    3. Que renvoie `mystère(-2)` ?
