# Affine par morceaux

On considère la fonction suivante :

```python
def mystère(x):
    if x < 0:
        return -x + 1
    elif x < 2:
        return -x + 7
    else:
        return 2*x + 1
```

=== "Questions"
    1. Que renvoie `mystère(0)` ?
    2. Que renvoie `mystère(2)` ?
    3. Que renvoie `mystère(-5)` ?
