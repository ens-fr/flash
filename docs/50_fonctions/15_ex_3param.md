# Mystère à 3 paramètres

On considère la fonction suivante :

```python
def mystère(a, b, c):
    if a > b:
        m = a
    else:
        m = b
    if m > c:
        return m
    else:
        return c
```

=== "Questions"
    1. Que renvoie `mystère(a=7, b=1, c=3)` ?
    2. Que renvoie `mystère(a=5, b=12, c=-2)` ?
    3. Que renvoie `mystère(a=2, b=4, c=12)` ?
    4. Quel est l'objectif de cette fonction ?
