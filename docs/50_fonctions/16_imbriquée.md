# Fonction qui appelle une autre

On considère les fonctions suivantes :

```python
def g(a):
    return 4*a*a

def f(x):
    return g(x) + 1
```

=== "Questions"
    1. Que renvoie `f(10) ?`
    2. Exprimer `f(x)` en fonction de `x`, sans utiliser `g`.
