# Double mystère

On considère les fonctions suivantes :

```python
def mystère_1(a, b):
    if a > 5:
        b = b - 5
    if b >= 10:
        b = b + 1
    return (a, b)

def mystère_2(a, b):
    if a > 5:
        b = b - 5
    elif b >= 10:
        b = b + 1
    return (a, b)
```

=== "Questions"
    1. Que renvoie `mystère_1(7, 20)` ?
    2. Que renvoie `mystère_2(7, 20)` ?
