# Fonction qui appelle une autre, qui appelle une autre

On considère les fonctions suivantes :

```python
def f1(a):
    return 4*a

def f2(x):
    return f1(x)**2

def f3(x):
    return f3(x) + 6
```

=== "Questions"
    1. Que renvoie `f3(2)` ?
    2. Exprimer `f3(x)` en fonction de `x`.
