# Mystère 2 paramètres

On considère la fonction suivante :

```python
def mystère(a, b):
    if a > 5:
        b = b - 5
    if b >= 10:
        b = b + 1
    return (a, b)
```

=== "Questions"
    1. Que renvoie `mystère(a=7, b=12)` ?
    2. Que renvoie `mystère(a=2, b=28)` ?
