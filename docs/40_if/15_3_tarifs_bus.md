# Prix billet de bus

Le prix d'un billet de bus est le suivant :

- 13 euros pour les jeunes (moins de 18 ans)
- 13 euros pour les seniors (plus de 60 ans) qui achètent au moins 4 tickets
- 15 euros pour les seniors qui achètent moins de 4 tickets
- 17 euros dans tous les autres cas

Une personne achète plusieurs billets, on voudrait le prix total.

1. La fonction suivante correspond-elle à cet énoncé ? Si non, corriger là.

```python
def prix_total(nb_billets, age):
    if age > 18:
        return nb_billets * 13
    elif (age >= 60) and (nb_billets >= 4):
        return nb_billets * 13
    elif age <= 60:
        return nb_billets * 15
    else:
        return nb_billets * 17
```

