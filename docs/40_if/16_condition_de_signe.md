# Condition de signe

On considère la fonction suivante :

```python
def mystère(x):
    if x < 0:
        return -x + 1
    else:
        return 2*x + 1
```

1. Que renvoie `mystère(-6)` ?
2. Que renvoie `mystère(4)` ?

