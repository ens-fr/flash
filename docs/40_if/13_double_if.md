# Mystère à deux `if`

On considère la fonction suivante :

```python
def mystère(x, y):
    if (x < 30) and (y == 4):
        return x
    if not(x + y < 20):
        return y
    else:
        return x + y
```

=== "Questions"
    1. Que renvoie `mystère(10, 30)` ?
    2. Proposer un couple `(x, y)` pour lequel `mystère(x, y)` renvoie la valeur de `x`.
    3. Proposer un couple `(x, y)` pour lequel `mystère(x, y)` renvoie la valeur de `x + y`.

