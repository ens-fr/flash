# Deux Tarifs pour des photocopies

Dans un atelier de reprographie, la photocopie est à 0,10 euro.

Mais pour plus de 20 photocopies, le prix unitaire est de 0,08 euro.

Compléter la fonction suivante permettant de calculer le prix à payer
en fonction du nombre de photocopies.

```python
def prix_à_payer(nb_photocopies):
    if nb_photocopies <= 20:
        return ...  # [1]
    else:
        return ...  # [2]
```

=== "Questions"
    1. Que placer avant `# [1]` ?
    2. Que placer avant `# [2]` ?
