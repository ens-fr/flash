# s égal s plus i

On considère la fonction suivante :

```python
def mystère(n):
    s = 0
    for i in range(n):
        s = s + i
    return s
```

=== "Questions"
    1. Que renvoie `mystère(5)` ?
    2. Vrai ou Faux : `mystère(n)` peut renvoyer `25`.

??? done "1. Solution"

    - `s` est initialisé à $0$.
    - On répète pour `i` allant de $0$ inclus à $5$ exclus : `s` augmente de `i`.
        - `s` devient $0 + 0 = 0$,
        - `s` devient $0 + 1 = 1$,
        - `s` devient $1 + 2 = 3$,
        - `s` devient $3 + 3 = 6$,
        - `s` devient $6 + 4 = 10$.
    - La fonction renvoie `10`.

    >Variante, avec le cours de première.
    >
    >`mystère(n)` renvoie $0 + 1 + 2 + ... + (n-1)$, et cette somme est égale à $\dfrac{n(n-1)}{2}$.
    >Pour $n=5$, `mystère(n)` renvoie $\dfrac{5×4}{2}$, donc `10`.

??? done "2. Solution"
    La fonction `mystère` est strictement croissante sur $\mathbb N$, et ses valeurs successives sont : $0, 1, 3, 6, 10, 15, 21, 28, \cdots$. La valeur $25$ est absente.
    Faux, `mystère(n)` ne peut pas renvoyer `25`.

    >Variante, avec le cours de première.
    >
    >On cherche à résoudre l'équation $\dfrac{n(n-1)}{2}=25$.
    >Elle s'écrit $n^2 -n = 50$, ou encore $n^2 - n - 50 = 0$, de discriminant $\delta = (-1)^2 - 4×1×(-50) = 201$, or $\sqrt{201}$ n'est pas entier, donc il n'y a pas de solution entière. 
    >Faux, `mystère(n)` ne peut pas renvoyer `25`.
