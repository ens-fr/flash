# Double boucle

On considère la fonction suivante :

```python
def mystère(a):
    for _ in range(7):
        for _ in range(2):
            a = a + 1
        a = a - 2
    return a
```

=== "Questions"
    Que renvoie `mystère(5)` ?
    Que renvoie `mystère(a)`, en général ?

??? done "1. Solution"

    - La première boucle intérieure augmente `a` de $2$.
    - L'instruction suivante diminue `a` de $2$.
    - Ainsi le premier tour de la boucle externe, ne modifie pas globalement `a`. Les six autres tours ne modifient pas globalement `a` non plus.
    - `mystère(5)` renvoie `5`.

??? done "2. Solution"
    `mystère(a)` renvoie `a` pour toute entrée `a` numérique. Toujours vrai quand `a` est entier.

    :warning: pour les puristes, c'est faux en général, il existe quelques rares valeurs, des flottants extrêmes, pour lesquels `mystère(a)` ne renvoie pas `a`.
