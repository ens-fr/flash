# For mystérieux

On considère la fonction suivante :

```python
def mystère_1(n):
    for i in range(5):
        n = n + 2
    return n

def mystère_2(n):
    for i in range(5):
        n = n + 2
    return i
```

=== "Questions"

    1. Que renvoie `mystère_1(0)` ?
    2. Que renvoie `mystère_1(15)` ?
    3. Que renvoie `mystère_2(0)` ?
    4. Que renvoie `mystère_2(15)` ?

??? done "1. Solution"
    `mystère_1(n)` ajoute $5$ fois $2$ à $n$, donc renvoie $n + 10$.
    `mystère_1(0)` renvoie `10`.

??? done "2. Solution"
    `mystère_1(n)` ajoute $5$ fois $2$ à $n$, donc renvoie $n + 10$.
    `mystère_1(15)` renvoie `25`.

??? done "3. Solution"
    `mystère_2(n)` ajoute $5$ fois $2$ à $n$, pour $i$ allant de $0$ inclus à $5$ exclus. Le dernier tour de boucle est avec $i=4$. Donc la fonction renvoie 4.
    `mystère_2(0)` renvoie `4`.

??? done "4. Solution"
    `mystère_2(n)` ajoute $5$ fois $2$ à $n$, pour $i$ allant de $0$ inclus à $5$ exclus. Le dernier tour de boucle est avec $i=4$. Donc la fonction renvoie `4`.
    `mystère_2(15)` renvoie `4`.
