# Répétitions d'instructions

Voici une fonction :

```python
def mystère():
    a = 1
    b = 2
    c = 3
    for i in range(4):
        a = a + 1
        b = i
        c = c + i
    return (a, b, c)
```

=== "Question"
    Que renvoie `mystère()` ?

??? done "1. Solution"
    À la fin de chacun des 4 tours de boucles, on a :

    1. $a = 1 + 1 = 2$ ; $b = 0$ ; $c = 3 + 0 = 3$
    2. $a = 2 + 1 = 3$ ; $b = 1$ ; $c = 3 + 1 = 4$
    3. $a = 3 + 1 = 4$ ; $b = 2$ ; $c = 4 + 2 = 6$
    4. $a = 4 + 1 = 5$ ; $b = 3$ ; $c = 6 + 3 = 9$
    
    La fonction renvoie `(5, 3, 9)`.
