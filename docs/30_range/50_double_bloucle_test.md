# Teste membres gauche et droit

On considère les fonctions suivantes :

```python
def membre_gauche(a, b):
    return (a + b)**2

def membre_droite(a, b):
    return a**2 + 2*a*b + b**2

def test(n, m):
    for i in range(n):
        for j in range(m):
            if gauche(i, j) != droite(i, j):
                return False
    return True
```

=== "Question"
    Que **fait** `test(20, 20)` ?

??? done "1. Solution"
    `test(2, 2)` teste pour tous les couples de nombres $(i, j)$ avec $i$ et $j$ entiers de $0$ inclus à $20$ exclus, si $(i+j)^2 \neq i^2 + 2ij + j^2$, ce qui n'arrive pas, ainsi `#!python return False` ne se produit pas.

    La fonction renvoie `#!python True`. On a vérifié une identité remarquable sur $20×20 = 400$ tests.
