# Types des variables

On considère le script suivant :

```python
a = 1
b = 3.5
c = "bonjour"
d = a + b
e = round(a, 2)
```

=== "Questions"
    Quel est le type de chacune des variables ?

??? done "Solution"

    - `a` est un entier (`int`).
    - `b` est un flottant (`float`).
    - `c` est une chaine de caractère (`string`).
    - `d` est un flottant, issu du calcul `1.0 + 3.5` ; le `1` entier est converti avant le calcul en flottant `1.0`.
    - `e` est un entier, comme `a`. `round` à un paramètre renvoie un entier. `round` à deux paramètres renvoie du même type que le premier paramètre.

    L'aide sur `round` s'obtient avec
    ```pycon
    >>> help(round)
    ```

    ```output
    Help on built-in function round in module builtins:

    round(number, ndigits=None)
        Round a number to a given precision in decimal digits.
        
        The return value is an integer if ndigits is omitted or None.  Otherwise
        the return value has the same type as the number.  ndigits may be negative.
    ```
