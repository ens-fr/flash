# Jeu de parenthèses

!!! tip "Rappel"
    * Python respecte les priorités.
    * `/` est la division dont le résultat est un flottant.
    * `//` est la division à quotient entier.


On considère des fonctions.

=== "Question 1"

    ```python
    def mystère_1(n):
        return 3*n - 5 / (n+1)
    ```

    Que renvoie `mystère_1(4)` ?

    ??? done "Solution"
        * `3*4 - 5/ (4+1)` s'évalue en `12 - 5/5`,
        * puis en `12 - 1.0`,
        * pour un résultat de `11.0` ; un nombre flottant.

=== "Question 2"

    ```python
    def mystère_2(n):
        return (3*n - 5) / n+1
    ```

    Que renvoie `mystère_2(5)` ?

    ??? done "Solution"
        
        * `(3*5 - 5) / 5+1` s'évalue en
        * `(15 - 5)/5 + 1`, puis en
        * `10/5 + 1`, puis en `2.0 + 1`,
        * pour un résultat de `3.0` ; un nombre flottant.

=== "Question 3"

    ```python
    def mystère_3(n):
        return (3*n - 5) / (n+1)
    ```

    Que renvoie `mystère_3(3)` ?

    ??? done "Solution"

        * `(3*3 - 5) / (3+1)` s'évalue en
        * `(9 - 5) / 4`, puis en
        * `4 / 4`,
        * pour un résultat de `1.0` ; un nombre flottant.

Vous pouvez vérifier :

{{ IDEv('exo02') }}
