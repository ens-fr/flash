def mystère_1(n):
    return 3*n - 5 / (n+1)

def mystère_2(n):
    return (3*n - 5) / n+1

def mystère_3(n):
    return (3*n - 5) / (n+1)


print("Question 1 ->", mystère_1(4))
print("Question 2 ->", mystère_2(5))
print("Question 3 ->", mystère_3(3))
