# 3 lignes

On considère la fonction suivante :

```python
def mystère(a, b):
    c = a
    a = b
    b = c
    return (a, b)
```

=== "Questions"
    1. Que renvoie `mystère(7, 12)` ?
    2. Quel est l'objectif de cette fonction ?

??? done "1. Solution"

    * `c` devient égal à $7$.
    * `a` devient égal à $12$.
    * `b` devient égal à $7$.
    * La fonction renvoie `(7, 12)`.

??? done "2. Solution"

    * Cette fonction renvoie le couple de paramètres, mais échangés (_swap_ en anglais).
    * Cette fonction utilise une variable intermédiaire.
