# Un programme de calcul

On considère la fonction suivante :

```python
def mystère(n):
    y = n + 3
    y = y**2
    y = y - 9
    return y
```

=== "Question"
    Que renvoie `mystère(4)` ?

??? done "Solution"

    - `y` devient $4+3 = 7$.
    - `y` devient $7^2 = 49$.
    - `y` devient $49- 9=40$.
    - La fonction renvoie `40`.
