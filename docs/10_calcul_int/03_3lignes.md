# Trois lignes

On considère la fonction suivante :

```python
def mystère(n):
    a = -2 * n
    a = a * a
    a = a - n * n
    return a
```

=== "Question"
    Que renvoie `mystère(3)` ?

    ??? done "Solution"

        - `a` devient $-2×3 = -6$.
        - `a` devient $(-6)^2 = 36$.
        - `a` devient $36 - 9 = 27$.
        - La fonction renvoie $27$.

        De manière générale, avec $n$ le paramètre **initial**,

        - `a` devient $-2n$.
        - `a` devient $(-2n)^2 = 4n^2$.
        - `a` devient $4n^2 - n^2 = 3n^2$.
        - La fonction renvoie $3n^2$.
