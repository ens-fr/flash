# Petite variante

On considère la fonction suivante :

```python
def mystère(a, b):
    a = a + b
    b = a - b
    a = a - b
    return (a, b)
```
=== "Question"
    Que renvoie `mystère(7, 12)` ?

    ??? done "Solution"

        - `a` devient $19$.
        - `b` devient $7$.
        - `a` devient $12$.
        - La fonction renvoie `(12, 7)`.

        Cette fonction échange deux **nombres**, sans avoir besoin d'une troisième variable.
