# Calcul rétrograde

On considère la fonction suivante :

```python
def mystère(n):
    a = 4 * n
    b = a - 3
    return b
```

=== "Question 1"
    Vrai ou Faux : `mystère(6)` renvoie `21` ?

    ??? done "Solution"

        - `a` devient $4×6 = 24$.
        - `b` devient $24 - 3 = 21$.
        - Vrai ; la fonction renvoie `21`.

=== "Question 2"
    Que renvoie `mystère(-5)` ?

    ??? done "Solution"

        - `a` devient $4×(-5) = -20$.
        - `b` devient $-20 - 3 = -23$.
        - La fonction renvoie `-23`.

=== "Question 3"
    On suppose que `mystère(n)` renvoie `13` ; quelle est la valeur de `n` ?

    ??? done "Solution"
        Il y a deux façons de faire :

        1. On fait du calcul rétrograde, étape par étape, en résolvant une équation simple à chaque étape.
        2. On établit, si possible, une expression littérale équivalente à la fonction mystère ; on résout une seule équation (peut-être difficile).

        ??? tip "Méthode 1"
            - La fonction a renvoyé `13`, donc
            - `b` valait $13$ à la fin, donc
            - $13 = a - 3$, d'où $a = 16$ juste avant, donc
            - $16 = 4×n$, d'où $n=\frac{16}4=4$ au départ.
            - Et réciproquement !
            - La valeur du paramètre `n` était `4`.
        
        ??? danger "Méthode 2"

            - La fonction renvoie globalement $4n - 3$. Ici, c'est simple !
            - On résout $13 = 4n - 3$, avec
            - $16 = 4n$, et enfin $n=4$.
            - Ici, c'était simple, mais en général c'est bien plus difficile. Il vaudra mieux remonter étape par étape.
        