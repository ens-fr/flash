# Modulo 2

On considère la fonction suivante :

```python
def mystère(n):
    if n % 2 == 0:
        return True
    else:
        return False
```

=== "Question 1"
    Que renvoie `mystère(8)` ?

    ??? done "Solution"
        $8$ modulo $2$ vaut zéro, donc la fonction renvoie `True`.

=== "Question 2"
    Que renvoie `mystère(15)` ?

    ??? done "Solution"
        $15$ modulo $2$ vaut un, donc la fonction renvoie `False`.

=== "Question 3"
    À quoi sert cette fonction ?

    ??? done "Solution"
        $n$ modulo $2$ vaut zéro, uniquement quand $n$ est pair, donc la fonction répond à la question « $n$ est-il pair ? » et renvoie le booléen associé (`True` ou `False`).
