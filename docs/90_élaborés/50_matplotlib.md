# Matplotlib mystère

On considère le script suivant :

```python
import matplotlib.pyplot as plt

def mystère((a, b), (c, d)):
    plt.plot([a, c], [b, d], "-")
    plt.show()
```

=== "Question"
    À quoi sert la fonction `mystère` ?

