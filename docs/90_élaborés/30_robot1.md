# Coordonnées du robot

On considère le script suivant :

```python
from random import random

def robot():
    """Renvoie les coordonnées d'un robot
    qui fait une marche aléatoire.
    """
    x, y = (0, 0)
    for i in range(3):
        a = random()
        if a < 0.5:
            y = y + 2
            x = x + 1
        else:
            x = x - 1
    return (x , y)
```

=== "Questions"
    1. On suppose que les 3 nombres aléatoires successifs renvoyés par `random()` sont `0.9` ; `0.3` et `0.5`. Que renvoie `robot()` ?
    2. Vrai ou Faux : `robot()` peut renvoyer `(0, 2)`.
    3. Vrai ou Faux : `robot()` peut renvoyer `(6, 3)`.
