# Points `matplotlib`

On considère le script suivant :

````python
import matplotlib.pyplot as plt

plt.plot([1, 2], [3, 6], "+")
plt.plot([4], [5], "o")
plt.show()
````

=== "Questions"
    1. Quels sont les coordonnées des points tracés ?
    2. Comment est représenté le point de coordonnées $(4, 5)$ ?

