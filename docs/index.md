# Questions Flash

> Ces exercices peuvent être utiles pour de la remédiation en classe, ou bien pour la pratique sur des automatismes.

```python
def ma_fonction(n):
    "Renvoie le double de `n` plus un."
    return 2*n + 1
```

Son utilisation en console :

```pycon
>>> ma_fonction(20):
41
```

{{ IDEv('index') }}

Ces exercices peuvent appuyer [le cours d'algorithmique, mathématiques avec Python](https://ens-fr.gitlab.io/algo1/).
